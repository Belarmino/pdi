import DCT

def main():
    print("EXECUTANDO DCT...")
    dct = DCT.DCT()
    iterator = 1
    while(iterator):
        print('\n\n\tTRABALHO PRATICO 2\n'
        '\t\t1 - IDCT\n' 
        '\t\t2 - COEFICIENTES IMPORTANTES 2\n'
        '\t\t3 - FILTRO PASSSA-BAIXA\n')
        op = int(input('\t OPÇÃO >> '))

        if op == 1:
            dct.idct_2d()

        elif op == 2:
            dct.coeficientes_importantes()

        elif op == 3:
            dct.passa_baixa()
        else:
            print('\nExit')
            iterator = 0
    

if __name__ == "__main__":
    main()