import numpy as np
import matplotlib.pyplot as plotagem
from PIL import Image
import cv2
import scipy.io.wavfile
import math
import time

class DCT:
    
    def __init__(self):
        self.imagem_original = Image.open("lena.bmp")
        self.imagem_frequencia = self.dct_2d()
        self.R = self.imagem_frequencia.shape[0]
        self.C = self.imagem_frequencia.shape[1]

    def dct_1d(self, vetor):
        N = len(vetor)
        X = np.zeros(N)
        aK = math.sqrt(2.0/N)
        N = len(vetor)

        for k in range(N):
            ck = math.sqrt(1.0/2.0) if k == 0 else 1
            somatorio = 0

            for n in range(N):
                a1 = 2.0 * math.pi * k * n
                a2 = k * math.pi
                nn = 2.0 * N

                somatorio += vetor[n] * math.cos((a1/nn) + (a2/nn))

            X[k] = aK * ck * somatorio

        return X

    def idct_1d(self, vetor):
        N = len(vetor)
        x = np.zeros(N)
        aK = math.sqrt(2.0/N)
        N = len(vetor)

        for n in range(N):
            somatorio = 0
            for k in range(N):
                a1 = 2.0 * math.pi * k * n
                a2 = k * math.pi
                nn = 2.0 * N
                cK = math.sqrt(1.0/2.0) if k == 0 else 1

                somatorio += cK * vetor[k] * math.cos((a1/nn) + (a2/nn))

            x[n] = aK * somatorio

        return x
    # TRANSFORMADA 2D 
    def dct_2d(self):
        #EXIBINDO ORIGINAL
        self.imagem_original.show()
        imagem = np.asarray(self.imagem_original)

        imagem_dct = np.zeros(imagem.shape)
        inicio = time.time()

        # APLICA DCT NAS LINHAS
        for indice, linha in enumerate(imagem):
            imagem_dct[indice] = self.dct_1d(linha)
 
        # TRASPÕE MATRIZ
        imagem_dct = imagem_dct.T
        
        # APLICA DCT NAS COLUNAS
        for indice, coluna in enumerate(imagem_dct):
            imagem_dct[indice] = self.dct_1d(coluna)

        fim = time.time()
        print("DCT Levou: {0:.2f} segundos".format((fim - inicio)))
        # RETORNANDO A MATRIZ SEM TRANSPOSIÇÃO
        imagem_dct = imagem_dct.T
        imagem_retorno = imagem_dct

        imagem_norm = imagem_dct.copy()
        # COLOCANDO OS COSSENOS COM O MODULO
        imagem_dct = self.normaliza_imagem(imagem_norm)
        # CALCULANDO COEFICIENTE DC
        imagem_dc = imagem_dct[0][0]
        print("Nivel DC: {0}".format(imagem_dc))

        # APAGA NÍVEL DC
        imagem_dct[0][0] = 0
        # EXIBE DOMINIO DA FREQUENCIA
        imagem_frequencia2 = Image.fromarray(imagem_dct)
        imagem_frequencia2.show()
        return imagem_retorno

    # TRANSFORMADA INVERSA
    def idct_2d(self):
        imagem_idct = np.zeros(self.imagem_frequencia.shape)
        inicio = time.time()

        # IDCT NAS LINHAS
        for indice, linha in enumerate(self.imagem_frequencia):
            imagem_idct[indice] = self.idct_1d(linha)

        imagem_idct = imagem_idct.T
        # IDCT NAS COLUNAS
        for indice, coluna in enumerate(imagem_idct):
            imagem_idct[indice] = self.idct_1d(coluna)

        fim = time.time()
        print("IDCT Levou: {0:.2f} segundos".format((fim - inicio)))

        # RETORNANDO A MATRIZ SEM TRANSPOSIÇÃO
        imagem_idct = imagem_idct.T
        imagem_retorno = Image.fromarray(imagem_idct)
        imagem_retorno.show()

    def idct_2d_coeficientes(self,imagem_coeficientes):
        imagem_idct = np.zeros(imagem_coeficientes.shape)
        inicio = time.time()

        # IDCT NAS LINHAS
        for indice, linha in enumerate(imagem_coeficientes):
            imagem_idct[indice] = self.idct_1d(linha)

        imagem_idct = imagem_idct.T
        # IDCT NAS COLUNAS
        for indice, coluna in enumerate(imagem_idct):
            imagem_idct[indice] = self.idct_1d(coluna)

        fim = time.time()
        print("IDCT Levou: {0:.2f} segundos".format((fim - inicio)))

        # RETORNANDO A MATRIZ SEM TRANSPOSIÇÃO
        imagem_idct = imagem_idct.T
        imagem_retorno = Image.fromarray(imagem_idct)
        imagem_retorno.show()

    def normaliza_imagem(self,imagem):
        for i in range(imagem.shape[0]):
            for j in range(imagem.shape[1]):
                imagem[i][j] = abs(imagem[i][j])
        return imagem

    # FILTRA OS COEFICIENTES MAIS IMPORTANTES
    def coeficientes_importantes(self):
        # PRECISA DO VETOR NORMALIZADO PARA SELECIONAR IMPORTANTES
        imagem_frequencia = self.normaliza_imagem(self.imagem_frequencia.copy())
        # PRECISA DO ORIGINAL PARA ARMAZENAR O RESULTANTE
        imagem_frequencia_original = self.imagem_frequencia.copy()
        # RECEBENDO QUANTIDADE DE FREQUENCIAS IMPORTANTES DOS USUÁRIOS
        numero_cos = int(input("Quantos coeficientes devem ser preservados >> "))
        if(numero_cos < 0 or numero_cos > (self.R*self.C)):
            return print("ERROR: NUMERO DE COEFICIENTES INVALIDO!!")

        # MATRIZ RESULTANTE
        imagem_cos_importantes = np.zeros(imagem_frequencia.shape)
        # TRANFORMA EM LISTA TRADICIONAL PARA ACESSO AO INDICE
        imagem_frequencia = imagem_frequencia.tolist()

        maximo = 0 
        index_maximo = 0
        iteracao = 0
        inicio = time.time()
        # REPETINDO ITERACAO ATÉ O NUMERO DE COSSENOS
        while(iteracao < numero_cos):
            # COLETANDO A MAXIMA FREQUENCIA
            for i in range(len(imagem_frequencia)):
                maximo_aux = max(imagem_frequencia[i])
                #ARMAZENA INDICE DO COSSENO PARA ARMAZENAR NO RESULTANTE
                index_aux_max = imagem_frequencia[i].index((max(imagem_frequencia[i])))
                if maximo_aux > maximo:
                    maximo = maximo_aux
                    index_maximoi,index_maximoj = i,index_aux_max
            # IMAGEM RESULTANTE RECEBE FREQUENCIA
            imagem_cos_importantes[index_maximoi][index_maximoj] = imagem_frequencia_original[index_maximoi,index_maximoj]
            # IMAGEM LIDA APAGA MAIOR FREQUENCIA
            imagem_frequencia[index_maximoi][index_maximoj] = 0 
            iteracao += 1
            maximo = 0
            index_maximoi = 0
            index_maximoj = 0
        fim = time.time()
        print("Mapeamento dos Cossenos Levou: {0:.2f} segundos".format((fim - inicio)))
        # NORMALIZANDO SO PARA EXIBICAO
        imagem_filtrada = Image.fromarray(self.normaliza_imagem(imagem_cos_importantes.copy()))
        imagem_filtrada.show()
        # RECONSTRUINDO NO ESPACO PARA VER O RESULTANDO
        self.idct_2d_coeficientes(imagem_cos_importantes)

    #FILTRO DE SUAVIZAÇÃO APLICANDO FILTRO NXN NOS PIXEIS DA IMAGEM NO DOM FREQUENCIA
    def passa_baixa(self):

        imagem_frequencia = self.imagem_frequencia.copy()
        # RECEBE TAMANHO DO FILTRO
        filter = int(input("Qual o tamanho do fitro passa-baixas >> "))
        if(filter < 0 or filter > self.R):
            return print("ERROR: FILTRO INVALIDO!!")
        # IMAGEM RESULTANTE
        imagem_filtrada = np.zeros(imagem_frequencia.shape)
        # SE COEFICIENTE LOCALIZADO COUBER NO FILTRO ADICIONA A MATRIZ RESULTANTE
        for i in range(len(imagem_frequencia)):
            if(i > filter):
                break
            for j in range(len(imagem_frequencia[i])):
                if(j > filter):
                    break
                else:
                    imagem_filtrada[i][j] = imagem_frequencia[i][j]
        # NORMALIZA APENAS PARA EXIBIÇÃO
        imagem_filtrada_freq = Image.fromarray(self.normaliza_imagem(imagem_filtrada.copy()))
        imagem_filtrada_freq.show()
        # PASSA PARA O DOMINIO DO ESPACO PARA VER O RESULTADO
        self.idct_2d_coeficientes(imagem_filtrada)

